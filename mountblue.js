/* eslint-disable no-console */
/* eslint-disable no-use-before-define */
/* eslint-disable no-unused-vars */
const program = require('commander');
const fs = require('fs');

const questions = ['what is your name', 'which programming langauge do u know', 'How much experience of programming do u have', 'Tell me about your hobbies', 'What type of company are you working right now..'];
const answers = [];
const path = require('path');

let response = '';
let fileName = '';
program.option('--init', '--i');
program.parse(process.argv);
// check if a fileName is present or not..
// if present print the contents of given fileName
// else create fileName
if (fs.readdirSync(__dirname).indexOf(fileName) !== -1) {
  fs.readFile(fileName, 'UTF-8', (error, data) => {
    if (error) {
      console.error(error);
    }
    process.stdout.write(data);
  });
} else if (program.init) {
  createFile();
} else {
  process.stdout.write('usage :node mountblue.js --init\n');
}

function createFile() {
  process.stdout.write(`${questions[answers.length]}\n`);

  process.stdin.on('data', (answer) => {
    if (answers.length === 0) {
      // create a fileName by UserName..
      fileName = answer.toString().trim();
      fileName = fileName.concat('.txt');
      response = fs.createWriteStream(`${fileName}`, 'UTF-8');
      response.write(`${questions[answers.length]}\n`);
    }
    answers.push(answer);
    response.write(`${answer}`);
    if (answers.length === questions.length) {
      process.exit();
    }
    process.stdout.write(`${questions[answers.length]}\n`);
    response.write(`${questions[answers.length]}\n`);
  });
  process.on('exit', () => {
    if (answers.length) {
      process.stdout.write(`The response has been stored in ${fileName}\n`);
    }
  });
}
fileName = process.argv[2].toString().trim();
if (fileName.indexOf('txt') === -1) {
  fileName = fileName.concat('.txt');
}
